package com.thein.expoapp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.cover_list_item.view.*

class CoversAdapter(context: Context) :
    RecyclerView.Adapter<CoversAdapter.ViewHolder>() {

    private lateinit var coverUrls: List<String>
    private val glide = Glide.with(context)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val coverUrl = coverUrls[position]
        glide
            .load(coverUrl)
            .into(holder.coverView)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.cover_list_item, parent, false)
        )
    }

    override fun onViewRecycled(holder: ViewHolder) {
        glide.clear(holder.coverView)
    }

    override fun getItemCount(): Int {
        return coverUrls.size
    }

    fun setCoversList(coversListUrls: List<String>) {
        coverUrls = coversListUrls
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val coverView: ImageView = itemView.cover
    }
}