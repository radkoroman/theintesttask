package com.thein.expoapp

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.thein.fileexhibitsloader.DataLoader
import com.thein.model.Exhibit
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var adapter: ExhibitsAdapter
    private val compositDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        adapter = ExhibitsAdapter(this)
        initRecycler(adapter)
        val disposable = fetchExhibits()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { adapter.setExhibitsList(it) },
                { Toast.makeText(this, "Failed to load exhibits list.", Toast.LENGTH_LONG).show() })
        compositDisposable.add(disposable)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositDisposable.dispose()
    }

    private fun initRecycler(adapter: ExhibitsAdapter) {
        itemsList.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        itemsList.adapter = adapter
    }

    private fun fetchExhibits(): Single<List<Exhibit>> {
        return Single.fromCallable { DataLoader(this).getExhibitList() }
    }
}
