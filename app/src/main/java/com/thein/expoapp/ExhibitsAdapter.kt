package com.thein.expoapp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.thein.model.Exhibit
import kotlinx.android.synthetic.main.exhibit_list_item.view.*

class ExhibitsAdapter(private val context: Context) : RecyclerView.Adapter<ViewHolder>() {

    private lateinit var items: List<Exhibit>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val adapter = CoversAdapter(context)
        val holder = ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.exhibit_list_item, parent, false),
            adapter
        )
        holder.coversListView.layoutManager = LinearLayoutManager(
            holder.coversListView.context,
            RecyclerView.HORIZONTAL,
            false
        )
        holder.coversListView.adapter = adapter

        return holder
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.titleView.text = item.title
        holder.coversAdapter.setCoversList(item.images)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setExhibitsList(exhibitsList: List<Exhibit>) {
        items = exhibitsList
    }

}

class ViewHolder(view: View, adapter: CoversAdapter) : RecyclerView.ViewHolder(view) {
    val coversAdapter = adapter
    val coversListView: RecyclerView = itemView.coversList
    val titleView: TextView = itemView.title
}