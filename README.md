Description: develop an application which displays information about objects at the exhibitions. The application consists of one screen with a list, each row of which is intended for displaying one object. Because the object can have several pictures, it is necessary to provide the possibility of changing them by using a horizontal scroll. Display the object's name on top of each photo.

The project should consist of 3 separate modules:

1. The Model module must contain only interfaces and the next classes:
- The Exhibit class, with two fields: title and images
- The ExhibitsLoader interface with one method: getExhibitList():List<Exhibit>   

2. The FileExhibitsLoader module, which contains only the implementation of the ExhibitsLoader interface by retrieving data from a JSON file https://goo.gl/t1qKMS (save it locally in assets before using)

3. An Application that receives the data from the FileExhibitsLoader and performs its displaying according to the technical task.