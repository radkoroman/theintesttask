package com.thein.model

interface ExhibitsLoader  {

    fun getExhibitList():List<Exhibit>

}