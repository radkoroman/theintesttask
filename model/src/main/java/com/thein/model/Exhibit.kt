package com.thein.model

data class Exhibit(val title: String, val images: List<String>)