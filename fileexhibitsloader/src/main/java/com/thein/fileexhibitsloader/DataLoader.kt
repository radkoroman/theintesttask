package com.thein.fileexhibitsloader

import android.content.Context
import com.google.gson.Gson
import com.thein.model.Exhibit
import com.thein.model.ExhibitsLoader

private const val DATA_FILE = "data.json"

class DataLoader(private val context: Context) : ExhibitsLoader {

    private data class Data(val list: List<Exhibit>)

    private val gson = Gson()

    override fun getExhibitList(): List<Exhibit> {
        return gson.fromJson(readJSONFromFile(), Data::class.java).list
    }

    private fun readJSONFromFile(): String {
        return context.assets.open(DATA_FILE).bufferedReader().use { it.readText() }
    }

}